---
title: VBA
tags:
  - Windows
  - Excel
  - Access
  - Office Addin
excerpt: 様々なOffice製品でVBAの開発経験があります
date: 2020-07-30T19:22:21.943Z
featuredImage: uploads/vba.png
---

# Excel VBA

- 社内の人力作業をツールを作成して自動化しました
- コピーペーストの軽作業から大規模の台帳の転記ツールなどを開発しました

# Access VBA

- 各部門で作成されたExcel帳票を取りまとめるツールを開発しました
- Excelのフォーマットの整理や表示したいデータのクエリなどを開発

# Outlook VBA

- Redmineによる問い合わせ管理システムとの連携のアドインを開発しました
- VBAからRedmineのAPIを利用しました
- Outlookからボタン一つで問い合わせ状況を更新できるよう対応しました