---
title: JavaScript
tags:
  - Node
  - Express
  - Vue
  - Svelte
  - Backend
  - Frontend
excerpt: フロントエンドのフレームワークによる開発経験が多いです
date: 2020-07-30T19:22:21.943Z
featuredImage: uploads/javascript.png
---

# Express

- backendのAPIを中心とした開発
- フロントエンド単体のリポジトリでテストを行うためモック用にAPIを開発しました

# Vue

- vue.cliによるnuxtやpwaを用いたアプリケーションを開発しました
- 開発途中のアプリケーションにCDNで組み込み徐々にvueに切り替えるような開発経験もあります
- 最近ではGridsomeを使用してJamstackによるCMSの作成やGraphQLを使用しています

# Svelte

- 比較的新しいフレームワークで簡単な間違い探しゲームを作りました
- 「Vue.js」との差をQiitaの記事にまとめました
- https://qiita.com/tamoco/items/8dae2a25ab38e83d90f5
