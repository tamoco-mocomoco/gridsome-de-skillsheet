---
title: Python
tags:
  - Backend
  - Flask
  - Django
  - Chainer
excerpt: 主にBackendのAPIを開発。機械学習はChainerを利用経験有。
date: 2020-07-30T21:58:14.606Z
featuredImage: uploads/python.png
---

# Flask

- BackendのAPIサーバーを中心に開発しました
- 基本的にはAPIを中心とした開発ですがJinjaによるフロント側の開発経験もあります

## Django

- BIツールであるTIBCO spotfireからR言語（PythonInR）でデータを取得しました
- BackendからFrontendまで１つのサーバーで管理するためDjangoを採用しました
- Chainerによる機械学習環境も同じサーバーで構築しました
