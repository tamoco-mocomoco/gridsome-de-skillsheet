---
title: C Sharp
tags:
  - Windows
  - Unity
  - Form Application
  - Office Addin
excerpt: Unityによるゲーム製作。Windows Form Applicationの開発経験があります
date: 2021-01-17T19:22:21.943Z
featuredImage: uploads/c-sharp.png
---

# Unityによるゲーム製作

- ゲームエンジンであるUnityで3Dや2Dのゲームを開発しました
- 作ったゲームアプリはAndroid・iOSを対応とするクロスプラットフォームのアプリになります
- AppStore・GooglePlayにリリースした経験があります


# Windows Form Application

- コンテンツの中身まで検索可能なファイル検索ツールを開発
- 共有フォルダに対しファイルが格納されたときに特定の処理を実行する監視ツールなどを開発しました


# Microsoft Office AddIn

- Microsoft Office Outlookで使用する誤送信を防ぐアドインを開発しました
- 社内の導入必須のツールとして開発され、全社員にインストールされるツールになります
- 不具合時に自動的にエラーメッセージを添付しメールを送付する機能の実装
- 誤送信以外にも誤送信につながる行動を未然に防ぐ機能などを実装しました
