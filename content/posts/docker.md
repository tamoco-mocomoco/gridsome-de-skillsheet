---
title: Container
tags:
  - Docker
  - Kubernetes
excerpt: コンテナ関連の技術を記載
date: 2020-07-30T19:22:21.943Z
featuredImage: uploads/docker.png
subImage: uploads/docker.png
---

# Docker

- 基本的にはdocker-composeを使用した環境構築が得意です
- 開発環境やデータ検証を行う上でスムーズに環境を準備することができます
- 最近ではNodeの環境も人によって依存することが多くなってきたためコンテナで準備するようにしました

## Kubernetes

- kubectlによる基本的なコマンドは実行できます
- DeploymentやCronJobなどのyamlの開発経験があります
- HelmChartを利用して必要なサービスを開発できます