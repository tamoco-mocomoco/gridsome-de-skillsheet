---
title: iOS Application
test: ああああ
tags:
  - Swift
  - Objective-C
excerpt: Objective-CとSwiftでアプリの開発を行いリリース経験もあります
date: 2020-07-30T19:22:21.943Z
featuredImage: uploads/ios.png
---

## Objective-C Apps

### もぐらたたき

- 初めてリリースしたシンプルなもぐらたたきゲームです
- 二重配列を学んだ延長で作成しました

<img 
 style="padding-left:20px"
 src="/swift-obj-c/mogura-tataki.jpg" 
 width="300px">

## Swift Apps

### 巫女さんおみくじアプリ

- 実際にスマホを振っておみくじを引くことができるジャイロ機能を使ったおみくじアプリです

<img
 style="padding-left:20px"
 src="/swift-obj-c/miko-omikuji.jpg"
 width="300px">

### はにわ鑑定士

- スワイプを操作で遊べる仕分けゲームです

<img
 style="padding-left:20px"
 src="/swift-obj-c/haniwa-checker.jpg"
 width="300px">