---
title: Java
tags:
  - Backend
  - Frontend
  - Docker
  - Android Studio
  - eclipse
  - Spring
excerpt: Spring研修プロジェクトの発足やAndroidのアプリ開発の経験などを記載。
date: 2020-07-30T22:01:10.795Z
featuredImage: uploads/java.png
---

# Spring研修用プロジェクト

- 開発未経験のエンジニアを開発現場に配属させるためにPJとして発足しました
- Javaの学習を目的にSpringを学習することになったエンジニアに対しサポートしました
- 開発における環境構築や動作確認のための手順などを作成しました



## Qiita

- https://qiita.com/tamoco/items/cf657ec4d74ccbf4d493

## GitLab

- https://gitlab.com/vorwort.k/yasuda-spring


## 実績記録とアーキテクチャ

<img src="/java/spring-arch.png">


# Android アプリケーション開発

eclipseによるAndroidアプリの開発を経験。eclipseからAndroid Studioに移行した後も開発経験があります