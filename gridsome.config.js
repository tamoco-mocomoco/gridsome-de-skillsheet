// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
module.exports = {
  siteName: 'tamoco skill sheet',
  plugins: [
    // Load all Blog Posts from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/posts/**/*.md',
        typeName: 'Post',
        refs: {
          tags: {
            typeName: 'Tag',
            create: true,
          },
        },
      },
    },

    // Load Authors from file system
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'content/about/**/*.md',
        typeName: 'About',
        refs: {
          posts: {
            typeName: 'Post',
          },
        },
      },
    },

  ],

  templates: {
    Post: [
      {
        // path: '/test/:year/:month/:day/:title',
        path: '/skill/:title',
        componenent: '~/templates/Post.vue',
      },
    ],
    Tag: [
      {
        path: '/tag/:title',
        componenent: '~/templates/Tag.vue',
      },
    ]
  },

  transformers: {
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        // ...global plugins
      ],
    },
  },
};

